import { Pool } from 'pg';

const config = {
  user: 'postgres',
  host: 'localhost',
  database: 'tailormeddb',
  password: 'qwe123',
  port: 5432
};

export const getDbConnection = async () => {
  try {
    const pool = new Pool(config);
    await pool.query(
      'CREATE TABLE IF NOT EXISTS patients (patient_id varchar(255), firstName varchar(255), lastName varchar(255), dob varchar(255), gender varchar(255), sex varchar(255), address varchar(255));'
    );
    await pool.query(
      'CREATE TABLE IF NOT EXISTS treatments (treatment_id varchar(255), patient_id varchar(255), startDate varchar(255), endDate varchar(255), diagnoses varchar(255), treatmentLine varchar(255), numberOfCycles varchar(255));'
    );

    return pool;
  } catch (error) {
    console.error({
      message: error.message,
      stack: error.stack
    });
  }
  return new Pool(config);
};
