export interface H1_Treatments {
  PatientID: number;
  StartDate: string;
  EndDate: string;
  Active: string;
  DisplayName: string;
  Diagnoses: string;
  TreatmentLine: string;
  CyclesXDays: string;
  TreatmentID: number;
}

export const h1_treatments_headers = [
  'PatientID',
  'StartDate',
  'EndDate',
  'Active',
  'DisplayName',
  'Diagnoses',
  'TreatmentLine',
  'CyclesXDays',
  'TreatmentID'
];
