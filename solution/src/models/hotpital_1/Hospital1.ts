import * as path from 'path';
import * as fs from 'fs';
import pgFormat from 'pg-format';
import { parse } from 'csv-parse';
import { Pool } from 'pg';

import { H1_Patients, h1_patients_headers } from './H1_Patients';
import { H1_Treatments, h1_treatments_headers } from './H1_Treatments';

export class Hostpital_1 {
  treatment_file_location: string;
  patient_file_location: string;
  patients: H1_Patients[];
  treatments: H1_Treatments[];
  db_connection: Pool;

  constructor(
    treatment_file_location: string,
    patient_file_location: string,
    dbConnection: Pool
  ) {
    this.treatment_file_location = treatment_file_location;
    this.patient_file_location = patient_file_location;
    this.patients = [];
    this.treatments = [];
    this.db_connection = dbConnection;
  }

  async readSourceFiles() {
    const fileReader = (fileName: string) =>
      fs.readFileSync(
        `${path.resolve(__dirname, `../../../data/${fileName}`)}`,
        { encoding: 'utf-8' }
      );

    parse(
      fileReader(this.patient_file_location),
      {
        delimiter: ',',
        columns: h1_patients_headers
      },
      (error, result: Record<string, H1_Patients>) => {
        if (error) {
          console.error(error);
        }
        this.patients = Object.keys(result).map((key) => result[key]);
        this.patients.shift(); // removing the headers row
        this.writePatients();
      }
    );
    parse(
      fileReader(this.treatment_file_location),
      {
        delimiter: ',',
        columns: h1_treatments_headers
      },
      (error, result: Record<string, H1_Treatments>) => {
        if (error) {
          console.error(error);
        }
        this.treatments = Object.keys(result).map((key) => result[key]);
        this.treatments.shift(); // removing the headers row
        this.writeTreatments();
      }
    );
  }

  async writePatients() {
    try {
      const values = Object.values(this.patients).map((patient) => {
        return [
          patient.PatientID,
          patient.FirstName,
          patient.LastName,
          patient.PatientDOB,
          patient.Gender,
          patient.Sex,
          patient.City + ' , ' + patient.State + ' , ' + patient.Address
        ];
      });
      this.db_connection.query(
        pgFormat(
          `INSERT INTO patients(patient_id, firstname, lastname, dob, gender, sex, address) VALUES %L`,
          Object.values(values)
        ),
        [],
        (err, result) => {
          if (err) {
            console.error({
              message: err.message,
              stack: err.stack,
              name: err.name
            });
          } else {
            console.log('succesfuly written to db');
            //
          }
        }
      );
    } catch (error) {
      console.log(error.message);
      console.log(error.stack);
    }
  }

  async writeTreatments() {
    try {
      const values = Object.values(this.treatments).map((treatment) => {
        return [
          treatment.TreatmentID,
          treatment.PatientID,
          treatment.StartDate,
          treatment.EndDate,
          treatment.DisplayName,
          treatment.TreatmentLine,
          treatment.CyclesXDays
        ];
      });
      this.db_connection.query(
        pgFormat(
          `INSERT INTO treatments(
            treatment_id, patient_id, startdate, enddate, diagnoses, treatmentline, numberofcycles) VALUES %L`,
          Object.values(values)
        ),
        [],
        (err, result) => {
          if (err) {
            console.error({
              message: err.message,
              stack: err.stack,
              name: err.name
            });
          } else {
            console.log('succesfuly written to db');
          }
        }
      );
    } catch (error) {
      console.log(error.message);
      console.log(error.stack);
    }
  }
}
