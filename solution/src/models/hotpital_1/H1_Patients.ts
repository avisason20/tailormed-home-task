export interface H1_Patients {
  PatientID: number;
  MRN: number;
  PatientDOB: string;
  IsDeceased: string;
  DOD_TS: any;
  LastName: string;
  FirstName: string;
  Gender: string;
  Sex: string;
  Address: string;
  City: string;
  State: string;
  ZipCode: string;
  LastModifiedDate: string;
}

export const h1_patients_headers = [
  'PatientID',
  'MRN',
  'PatientDOB',
  'IsDeceased',
  'DOD_TS',
  'LastName',
  'FirstName',
  'Gender',
  'Sex',
  'Address',
  'City',
  'State',
  'ZipCode',
  'LastModifiedDate'
];
