export interface H2_Treatments {
  TreatmentId: string;
  PatientId: number;
  ProtocolID: number;
  StartDate: string;
  EndDate: string;
  Status: string;
  DisplayName: string;
  AssociatedDiagnoses: string;
  NumberOfCycles: number;
}

export const h2_treatments_headers = [
  'TreatmentId',
  'PatientId',
  'ProtocolID',
  'StartDate',
  'EndDate',
  'Status',
  'DisplayName',
  'AssociatedDiagnoses',
  'NumberOfCycles'
];
