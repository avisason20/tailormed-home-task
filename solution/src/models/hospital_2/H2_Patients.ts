export interface H2_Patients {
  PatientId: number;
  MRN: number;
  PatientDOB: string;
  IsPatientDeceased: string;
  DeathDate: any;
  LastName: string;
  FirstName: string;
  Gender: string;
  Sex: string;
  AddressLine: string;
  AddressCity: string;
  AddressState: string;
  AddressZipCode: string;
}

export const h2_patients_headers = [
  'PatientId',
  'MRN',
  'PatientDOB',
  'IsPatientDeceased',
  'DeathDate',
  'LastName',
  'FirstName',
  'Gender',
  'Sex',
  'AddressLine',
  'AddressCity',
  'AddressState',
  'AddressZipCode'
];
