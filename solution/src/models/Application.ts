import express from 'express';
import { Server } from 'http';

export interface Application {
  app: express.Application;
  server: Server;
}
