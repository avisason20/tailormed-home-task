export enum CustomRequestHeaders {
  requestId = 'requestId',
  requestingActorId = 'requestingActorId',
  someHeader = 'someHeader'
}

export interface Configuration {
  serverPort: string;
  logLevel: string;
  currentEnv: string;
  db_user: string;
  db_host: string;
  db_name: string;
  db_password: string;
  db_port: number;
}

export const getConfiguration = (
  env: NodeJS.ProcessEnv = process.env
): Configuration => ({
  serverPort: env.SERVER_PORT || '5000',
  logLevel: env.LOG_LEVEL || 'debug',
  currentEnv: env.CURRENT_ENV || 'local',
  db_user: env.DB_USER || 'postgres',
  db_host: env.DB_HOST || 'localhost',
  db_name: env.DB_NAME || 'tailormeddb',
  db_password: env.DB_PASSWORD || 'qwe123',
  db_port: parseInt(env.DB_PORT) || 5432
});
