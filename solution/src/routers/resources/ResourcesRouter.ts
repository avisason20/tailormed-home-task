import express,{ Router } from "express";
import path from "path";

const getResourcesRouter = () : Router => {
        const router = Router();

        router.use('/',express.static(path.resolve(__dirname,'../../../client/build')));

        router.get('/', (req,res)=> {
            res.redirect('/index.html');
        });

        return router;
}
export default getResourcesRouter;