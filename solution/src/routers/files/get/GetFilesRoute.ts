// import { getAllTodoItems } from 'buisness-logic/todo/get/GetAllTodoItems';
import { Hostpital_1 } from '@models/hotpital_1/Hospital1';
import { Hostpital_2 } from '@models/hospital_2/Hospital2';
import { getDbConnection } from 'connect';
import { Request, Response } from 'express';

export const getFilesRoute = () => async (
  request: Request,
  response: Response
) => {
  try {
    const dbConnection = await getDbConnection();

    const firstHospital = new Hostpital_1(
      'hospital_1_Treatment.csv',
      'hospital_1_Patient.csv',
      dbConnection
    );
    const secondHospital = new Hostpital_2(
      'hospital_2_Treatment.csv',
      'hospital_2_Patient.csv',
      dbConnection
    );
    await firstHospital.readSourceFiles();
    await secondHospital.readSourceFiles();

    return response
      .status(200)
      .send('successfuly updated db frome external sources');
  } catch (error) {
    return response.status(error.status).send(error.message);
  }
};
