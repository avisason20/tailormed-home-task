import { Router } from 'express';

import { getFilesRoute } from './get/GetFilesRoute';

const getFilesRouter = (): Router => {
  const todoRouter = Router();

  todoRouter.post('/updateDb', getFilesRoute()); // this is a post request because new information is being inserted to the db

  return todoRouter;
};

export default getFilesRouter;
