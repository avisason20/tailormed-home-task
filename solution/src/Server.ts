//@ts-ignore
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0; //allow us to make HTTPS requests without caring about the other side

import dotenv from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';

import getFilesRouter from '@routers/files/FilesRouter';
import { Application } from '@models/Application';
import getResourcesRouter from '@routers/resources/ResourcesRouter';
import { getLogMiddleware } from '@middleware/LogMiddleware';

import {
  getConfiguration,
  Configuration
} from '../src/configuration/Configuration';
import { initLogger } from '../src/configuration//logging/Logger';
import { Hostpital_1 } from '@models/hotpital_1/Hospital1';
import { Hostpital_2 } from '@models/hospital_2/Hospital2';
import { getDbConnection } from 'connect';

// export const dbConnector = admin.firestore();

const writeServerInfo = (serverPort: string) => {
  console.log(`Server is running on port ${serverPort}`);
};

export const initiateApp = async (
  env?: NodeJS.ProcessEnv
): Promise<Application> => {
  dotenv.config(); //enable reading environment variables from .env file
  const app: express.Application = express();
  const configuration: Configuration = getConfiguration(env);

  initLogger(configuration.logLevel, configuration.currentEnv);

  app.use(bodyParser());
  app.use(getLogMiddleware());
  app.use(getResourcesRouter());

  app.use('/files', getFilesRouter());

  console.log({
    message: 'Application will run with this configuration',
    configuration
  });

  const server = app.listen(configuration.serverPort, () => {
    writeServerInfo(configuration.serverPort);
  });

  return { app, server };
};
