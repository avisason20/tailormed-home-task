### This is the ChangeLog of TailorMed HA task

### All of the changes were made in different commits.

## Scaleability:

    # This solution implements scailabilty for the fact adding new data source will
      only takes the implemention of :
        - new class
        - data interface
        - read and write function to parse the data.

    # Currently connected to postgress localhost db.

## Things i would have changed if I had more time

- Creating client that get zip file containing patients and treatments files using
  content disposition
- Uniting both Hospital1 and Hospital 2 into single Hospital Class, with the types and headers
  given as params.
- Instead of hard coding calling every class, use some kind of design pattern.
